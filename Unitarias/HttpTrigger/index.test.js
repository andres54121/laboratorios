const httpFunction = require('./index');
const context = require('../testing/Context');

test('Http trigger example', async()=> {
    const request = {
        query: { name: 'Hitori Bocchi' }
    };

    await httpFunction(context, request);
    
    expect(context.res.body).toEqual('Welcome, Hitori Bocchi');

    expect(context.log.mock.calls.length).toBe(1);
}); 