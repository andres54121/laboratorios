const httpFunction = require('./index');
const context = require('../testing/Context');

test('Http trigger example', async()=> {
    const request = {
        query: { name: 'Hitori Bocchi' }
    };
    // PRUEBA DE RENDIMIENTO
    
    var interations = 1000000;
    console.time('FUNCTION #1');
    for(var i = 0; i < interations; i++){
        httpFunction(context, request);        
    }
    console.timeEnd('FUNCTION #1')
    
    expect(context.res.body).toEqual('Welcome, Hitori Bocchi');
    expect(context.log.mock.calls.length).toBe(interations);
}); 